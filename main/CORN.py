import sys
import os
import csv
import pathlib
import datetime

os.system("")

COLOR = {"HEADER": "\033[95m",
         "YELLOW": "\033[93m",
         "GREEN": "\033[92m",
         "RED": "\033[91m",
         "ENDC": "\033[0m",
         }

paTh = pathlib.Path("task.csv").resolve()


def welcome():
    print('Welcome to C.O.R.N!\n', COLOR["HEADER"])
    print('C.O.R.N , Command line Organized Records Notebook, is a multi-platform ToDo List')
    print('To display your todo list type "Display"\n'
          'To add a new task type "Add"\n'
          'To remove a task type "Remove"\n'
          'To edit a task type "edit"\n'
          'To display the commands again type "Help"\n'
          'To exit the program type "Exit" or "Quit"\n', COLOR["ENDC"])


def Menu(paTh):
    welcome()
    loop = True
    while loop:
        print('What can we help you with today?\n')
        command = input()
        command = command.lower()
        command = command.strip()

        if command == 'display':
            print('Your File\n')
            displayTasks(paTh)

        elif command == 'add':
            addTask(paTh)

        elif command == 'remove':
            removeTask(paTh)

        elif command == 'edit':
            editTask(paTh)
        
        elif command == 'clear':
            clearTask(paTh)
            
        elif command == 'displayhigh':
            displayHIGH(paTh)
            
        elif command == 'displaymid':
            displayMOD(paTh)
            
        elif command == 'displaylow':
            displayLOW(paTh)

        elif command == 'help':
            print('To enter a command please type one of the examples bellow\n'
              'To display your todo list type "Display"\n'
              'To display only tasks of high priority type "DisplayHigh"\n'
              'To display only tasks of moderate priority type "DisplayMid"\n'
              'To display only tasks of low priority type "DisplayLow"\n'
              'To add a new task type "Add"\n'
              'To remove a task type "Remove"\n'
              'To clear all tasks type "Clear"\n'
              'To edit a task type "Edit"\n'
              'To display the commands again type "Help"\n')

        elif command == 'exit' or command == 'quit':
            print('Good bye! Hope to see you again soon\n')
            loop = False

        else:
            print('Not a valid command, check spelling and enter again')

def clearTask(paTh):
    listT = []

    with open(paTh, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            listT.append(row)
    with open(paTh, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(listT[0])
    print('List Cleared\n')

def addTask(paTh):
    StoredPri = ["high", "moderate", "low", "HIGH", "MODERATE", "LOW"]
    with open(paTh, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        name = input("Enter task name: ")
        today = datetime.date.today()
        task = input("Enter description: ")
        loop1 = True
        while loop1:
            priority = input("Enter a priority level(high, moderate, low): ")
            priority.lower()

            # Ensures that priority input can only be one from StoredPri
            if priority in StoredPri:
                print("priority set")
                break
            else:
                print("Invalid input, please set the priority level")

        # Ensures that the user can only enter a date in the specified format
        while True:
            isdue = input("Enter due date: ")
            try:
                due_date = datetime.datetime.strptime(isdue, "%m/%d/%Y")
            except ValueError:
                print("Error: must be format mm/dd/yyyy")
            if due_date.date() < today:
                print("Due date can't be set to before today's date")
            else:
                csvwriter.writerow([name.strip(), today.strftime("%m/%d/%Y"), task, priority.strip().upper(),
                                    due_date.strftime("%m/%d/%Y")])
                print('Adding a task\n')
                break


def displayTasks(paTh):
    Rows = []
    num_rows = 0
    with open(paTh, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        Columns = next(csvreader)
        for row in csvreader:
            Rows.append(row)
        print('Field names are:\n' + ', '.join(field for field in Columns))

        for row in Rows:
            num_rows += 1
            # parsing each column of a row
            for col in row:
                if col == 'LOW':
                    print(num_rows, COLOR["GREEN"], row, COLOR["ENDC"])

                if col == 'MODERATE':
                    print(num_rows, COLOR["YELLOW"], row, COLOR["ENDC"])

                if col == 'HIGH':
                    print(num_rows, COLOR["RED"], row, COLOR["ENDC"])


def displayLOW(paTh):
    Rows = []
    num_rows = 0
    with open(paTh, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        Columns = next(csvreader)
        for row in csvreader:
            Rows.append(row)
        print('Field names are:\n' + ', '.join(field for field in Columns))

        for row in Rows:
            num_rows += 1
            # parsing each column of a row
            for col in row:
                if col == 'LOW':
                     print( COLOR["GREEN"], row, COLOR["ENDC"])


def displayMOD(paTh):
    Rows = []
    num_rows = 0
    with open(paTh, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        Columns = next(csvreader)
        for row in csvreader:
            Rows.append(row)
        print('Field names are:\n' + ', '.join(field for field in Columns))

        for row in Rows:
            num_rows += 1
            # parsing each column of a row
            for col in row:

                if col == 'MODERATE':
                    print( COLOR["YELLOW"], row, COLOR["ENDC"])


def displayHIGH(paTh):
    Rows = []
    num_rows = 0
    with open(paTh, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        Columns = next(csvreader)
        for row in csvreader:
            Rows.append(row)
        print('Field names are:\n' + ', '.join(field for field in Columns))

        for row in Rows:
            num_rows += 1
            # parsing each column of a row
            for col in row:
                if col == 'HIGH':
                    print( COLOR["RED"], row, COLOR["ENDC"])



def removeTask(paTh):
    listT = []
    userIn = int(input("Write the ID number of the task you would like to delete: "))
    i = 0
    j = 0

    with open(paTh, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            listT.append(row)
        for row in listT:
            if i is userIn:
                listT.remove(row)
            i += 1
    with open(paTh, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(listT[0])
    with open(paTh, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        while j != len(listT):
            if j > 0 and len(listT[j]) != 0:
                csvwriter.writerow(listT[j])
            j += 1


def editTask(infile):
    StoredPri = ["HIGH", "MODERATE", "LOW"]
    listT = []
    exSt = []
    userIn = int(input("Write the ID number of the task you would like to edit: "))
    i = 0
    k = 0
    j = 0
    lenL = 0
    with open(infile, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            if i is userIn:
                for col in row:
                    exSt.append(col)
            listT.append(row)
            i += 1
        lenL = len(listT)
        # print(exSt)
        for row in listT:
            if k is userIn:
                listT.remove(row)
            k += 1

    # Note: listT has everything except the chosen item

    ValueChange = 0
    val = None
    Option = input("Choose which column you wish to edit(NAME, TASK, PRIORITY): ").upper().strip()
    if Option == "NAME":
        val = 0
        name = input("Enter the new name for the task: ")
        ValueChange = name

    '''elif Option == "DATE":
        val = 1
        while True:
            created = input("Enter new date: ")
            try:
                created = datetime.datetime.strptime(created, "%m/%d/%Y")
                ValueChange = created
                break
            except ValueError:
                print("Error: must be format mm/dd/yyyy ")'''

    if Option == "TASK":
        val = 2
        Task = input("Enter the new description for the task: ")
        ValueChange = Task

    elif Option == "PRIORITY":
        val = 3
        Priority = input("Enter new priority level: ").upper().strip()
        if Priority in StoredPri:
            ValueChange = Priority
            print("priority set")

        else:
            print("Invalid input, please set the priority level")

    elif Option == "DUE":
        val = 4
        while True:
            Due = input("Enter new due Date: ")
            try:
                Due = datetime.datetime.strptime(Due, "%m/%d/%Y")
                ValueChange = Due
                break
            except ValueError:
                print("Error: must be format mm/dd/yyyy ")

    exSt[val] = ValueChange
    listT.insert(userIn, exSt)

    with open(infile, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(listT[0])
    with open(infile, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        while j != lenL:
            if j > 0:
                csvwriter.writerow(listT[j])
            j += 1



if (len(sys.argv) > 1):
    func = sys.argv[1]
    if func == "menu" or func == "Menu" or func == "MENU":
        Menu(paTh)
    elif func == "edit" or func == "Edit" or func == "EDIT" or func == "edittask" or func == "editTask" or func == "EditTask" or func == "EDITTASK":
        editTask(paTh)
    elif func == "displaylow" or func == "DisplayLow" or func == "DISPLAYLOW":
        displayLOW(paTh)
    elif func == "displaymid" or func == "DisplayMid" or func == "DISPLAYMID":
        displayMOD(paTh)
    elif func == "displayhigh" or func == "DisplayHigh" or func == "DISPLAYHIGH":
        displayHIGH(paTh)
    elif func == "clear" or func == "Clear" or func == "CLEAR":
        clearTask(paTh)
    elif func == "display" or func == "Display" or func == "DISPLAY":
        displayTasks(paTh)
    elif func == "add" or func == "addtask" or func == "Addtask" or func == "Add" or func == "ADD" or func == "AddTask" or func == "ADDTASK":
        addTask(paTh)
    elif func == "remove" or func == "Remove" or func == "REMOVE" or func == "delete" or func == "Delete" or func == "DELETE" or func == "removetask" or func == "Removetask" or func == "RemoveTask" or func == "REMOVETASK":
        removeTask(paTh)
    elif func == "help" or func == "Help" or func == "HELP" or func == "":
        print('To enter a command please type CORN followed by one of the commands bellow\n'
              'To display your todo list type "Display"\n'
              'To display only tasks of high priority type "DisplayHigh"\n'
              'To display only tasks of moderate priority type "DisplayMid"\n'
              'To display only tasks of low priority type "DisplayLow"\n'
              'To add a new task type "Add"\n'
              'To remove a task type "Remove"\n'
              'To clear all tasks type "Clear"\n'
              'To edit a task type "Edit"\n'
              'To display the commands again type "Help"\n')
    else:
        print('Not a valid command, check spelling and enter again')
else:
    print('To enter a command please type CORN followed by on the the commands bellow\n'
          'To display your todo list type "Display"\n'
          'To add a new task type "Add"\n'
          'To remove a task type "Remove"\n'
          'To display the commands again type "Help"\n')
