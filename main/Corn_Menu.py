"""
Ryan Horan
Aidan Huchro
Benji Hyde
Daniel Lopez
"""
import os
import csv
import datetime
import pathlib
import pandas as pd


os.system("")  # enables ansi colors on Windows cmd
# global variables

infile = pathlib.Path("task.csv").resolve()

COLOR = {"HEADER": "\033[95m",
         "YELLOW": "\033[93m",
         "GREEN": "\033[92m",
         "RED": "\033[91m",
         "ENDC": "\033[0m",
         }


# define all functions here
def menu():
    print('Welcome to C.O.R.N!\n', COLOR["HEADER"])
    print('C.O.R.N , Command line Organized Records Notebook, is a multi-platform ToDo List')
    print('To display your todo list type "Display"\n'
          'To add a new task type "Add"\n'
          'To remove a task type "Remove"\n'
          'To clear a task type "Clear"\n'
          'To edit a task type "Edit"\n'
          'To display the commands again type "Help"\n'
          'To exit the program type "Exit" or "Quit"\n', COLOR["ENDC"])


def addTask(infile):
    StoredPri = ["HIGH", "MODERATE", "LOW"]
    with open(infile, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        name = input("Enter task name: ")
        Date = datetime.date.today()
        task = input("Enter description: ")
        loop1 = True
        while loop1:
            priority = input("Enter a priority level(high, moderate, low): ").strip().upper()
            # priority.upper().strip()

            # Ensures that priority input can only be one from StoredPri
            if priority in StoredPri:
                print("priority set")
                break
            else:
                print("Invalid input, please set the priority level")

        # Ensures that the user can only enter a date in the specified format
        while True:
            due = input("Enter due date: ")
            try:
                due = datetime.datetime.strptime(due, "%m/%d/%Y")
                break
            except ValueError:
                print("Error: must be format mm/dd/yyyy ")

        csvwriter.writerow(
            [name.strip(), Date.strftime("%m/%d/%Y"), task, priority.strip().upper(), due.strftime("%m/%d/%Y"),
             dateCalc(infile)])
        print('Adding a task\n')


def dateCalc(infile):
    Calc = pd.read_csv(infile)
    date_format = "%m/%d/%Y"
    for i in range(len(Calc)):
        date1 = datetime.datetime.strptime(Calc['DUE'][i], date_format)
        date2 = datetime.datetime.now()
        delta = date1 - date2
        Calc['TIME LEFT'][i] = str(delta.days) + " Days left"
    Calc.to_csv(infile, index=False)


def displayTasks(infile):
    dateCalc(infile)
    Rows = []
    num_rows = 0
    with open(infile, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        Columns = next(csvreader)
        for row in csvreader:
            Rows.append(row)
        print('Field names are:\n' + ', '.join(field for field in Columns))

        for row in Rows:
            num_rows += 1
            # parsing each column of a row
            for col in row:
                if col == 'LOW':
                    print(num_rows, COLOR["GREEN"], row, COLOR["ENDC"])

                if col == 'MODERATE':
                    print(num_rows, COLOR["YELLOW"], row, COLOR["ENDC"])

                if col == 'HIGH':
                    print(num_rows, COLOR["RED"], row, COLOR["ENDC"])


def removeTask(infile):
    listT = []
    userIn = int(input("Write the ID number of the task you would like to delete: "))
    i = 0
    j = 0

    with open(infile, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            listT.append(row)
        for row in listT:
            if i is userIn:
                listT.remove(row)
            i += 1
    with open(infile, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(listT[0])
    with open(infile, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        while j != len(listT):
            if j > 0 and len(listT[j]) != 0:
                csvwriter.writerow(listT[j])
            j += 1
    print('removing a task\n')


def clearTask(infile):
    listT = []

    with open(infile, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            listT.append(row)
    with open(infile, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(listT[0])
    print('List Cleared\n')


def editTask(infile):
    StoredPri = ["HIGH", "MODERATE", "LOW"]
    listT = []
    exSt = []
    userIn = int(input("Write the ID number of the task you would like to edit: "))
    i = 0
    k = 0
    j = 0
    lenL = 0
    with open(infile, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            if i is userIn:
                for col in row:
                    exSt.append(col)
            listT.append(row)
            i += 1
        lenL = len(listT)
        for row in listT:
            if k is userIn:
                listT.remove(row)
            k += 1
    ValueChange = 0
    val = None
    Option = input("Choose which column you wish to edit(NAME, DATE, TASK, PRIORITY,DUE): ").upper().strip()
    if Option == "NAME":
        val = 0
        name = input("Enter the new name for the task: ")
        ValueChange = name

    """elif Option == "DATE":
        val = 1
        while True:
            created = input("Enter new date: ")
            try:
                created = datetime.datetime.strptime(created, "%m/%d/%Y")
                ValueChange = created
                break
            except ValueError:
                print("Error: must be format mm/dd/yyyy ")"""

    if Option == "TASK":
        val = 2
        Task = input("Enter the new description for the task: ")
        ValueChange = Task

    elif Option == "PRIORITY":
        val = 3
        Priority = input("Enter new priority level: ").upper().strip()
        if Priority in StoredPri:
            ValueChange = Priority
            print("priority set")

        else:
            print("Invalid input, please set the priority level")

    elif Option == "DUE":
        val = 4
        while True:
            Due = input("Enter new due Date: ")
            try:
                Due = datetime.datetime.strptime(Due, "%m/%d/%Y")
                ValueChange = Due.strftime("%m/%d/%Y")
                break
            except ValueError:
                print("Error: must be format mm/dd/yyyy ")

    exSt[val] = ValueChange

    with open(infile, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(listT[0])
    with open(infile, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        while j != lenL:
            if j > 0:
                if j is userIn:
                    csvwriter.writerow(exSt)
                else:
                    csvwriter.writerow(listT[(j)])
            j += 1


menu()
loop = True
while loop:
    print('What can we help you with today?\n')
    command = input()
    command = command.lower()
    command = command.strip()

    if command == 'display':
        print('Your File\n')
        displayTasks(infile)

    elif command == 'add':
        addTask(infile)

    elif command == 'remove':
        removeTask(infile)

    elif command == 'clear':
        clearTask(infile)

    elif command == 'edit':
        editTask(infile)

    elif command == 'help':
        print('To display your todo list type "Display"\n'
              'To add a new task type "Add"\n'
              'To remove a task type "Remove"\n'
              'DATECALC EXPERIMENT "datecalc"\n'
              'To edit a task type "edit" (not functional at the moment)\n'
              'To exit the program type "Exit" or "Quit"\n'
              'To display the commands again type "Help"\n')

    elif command == 'exit' or command == 'quit':
        print('Good bye! Hope to see you again soon\n')
        exit(1)

    else:
        print('Not a valid command, check spelling and enter again')
