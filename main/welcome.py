import sys
import os
import csv
import pathlib
import datetime

os.system("")

COLOR = {"HEADER": "\033[95m",
         "YELLOW": "\033[93m",
         "GREEN": "\033[92m",
         "RED": "\033[91m",
         "ENDC": "\033[0m",
         }

paTh = pathlib.Path("task.csv").resolve()
paThx = pathlib.Path("Corn_Menu.exe").resolve()


def Menu(paThx):
    if sys.platform == "win32":
        os.startfile(paThx)
    else:
        os.system("./Corn_Menu")
        sys.exit()


def addTask(paTh):
    StoredPri = ["high", "moderate", "low", "HIGH", "MODERATE", "LOW"]
    with open(paTh, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        name = input("Enter task name: ")
        today = datetime.date.today()
        task = input("Enter description: ")
        loop1 = True
        while loop1:
            priority = input("Enter a priority level(high, moderate, low): ")
            priority.lower()

            # Ensures that priority input can only be one from StoredPri
            if priority in StoredPri:
                print("priority set")
                break
            else:
                print("Invalid input, please set the priority level")

        # Ensures that the user can only enter a date in the specified format
        while True:
            isdue = input("Enter due date: ")
            try:
                due_date = datetime.datetime.strptime(isdue, "%m/%d/%Y")
            except ValueError:
                print("Error: must be format mm/dd/yyyy")
            if due_date.date() < today:
                print("Due date can't be set to before today's date")
            else:
                csvwriter.writerow([name.strip(), today.strftime("%m/%d/%Y"), task, priority.strip().upper(),
                                    due_date.strftime("%m/%d/%Y")])
                print('Adding a task\n')
                break


def displayTasks(paTh):
    Rows = []
    num_rows = 0
    with open(paTh, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        Columns = next(csvreader)
        for row in csvreader:
            Rows.append(row)
        print('Field names are:\n' + ', '.join(field for field in Columns))

        for row in Rows:
            num_rows += 1
            # parsing each column of a row
            for col in row:
                if col == 'LOW':
                    print(num_rows, COLOR["GREEN"], row, COLOR["ENDC"])

                if col == 'MODERATE':
                    print(num_rows, COLOR["YELLOW"], row, COLOR["ENDC"])

                if col == 'HIGH':
                    print(num_rows, COLOR["RED"], row, COLOR["ENDC"])


def removeTask(paTh):
    listT = []
    userIn = int(input("Write the ID number of the task you would like to delete: "))
    i = 0
    j = 0

    with open(paTh, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            listT.append(row)
        for row in listT:
            if i is userIn:
                listT.remove(row)
            i += 1
    with open(paTh, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(listT[0])
    with open(paTh, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        while j != len(listT):
            if j > 0 and len(listT[j]) != 0:
                csvwriter.writerow(listT[j])
            j += 1


if (len(sys.argv) > 1):
    func = sys.argv[1]
    if func == "menu" or func == "Menu" or func == "MENU":
        Menu(paThx)
    elif func == "display" or func == "Display" or func == "DISPLAY":
        displayTasks(paTh)
    elif func == "add" or func == "addtask" or func == "Addtask" or func == "Add" or func == "ADD" or func == "AddTask" or func == "ADDTASK":
        addTask(paTh)
    elif func == "remove" or func == "Remove" or func == "REMOVE" or func == "delete" or func == "Delete" or func == "DELETE" or func == "removetask" or func == "Removetask" or func == "RemoveTask" or func == "REMOVETASK":
        removeTask(paTh)
    elif func == "help" or func == "Help" or func == "HELP" or func == "":
        print('To enter a command please type CORN followed by on the the commands bellow\n'
              'To display your todo list type "Display"\n'
              'To add a new task type "Add"\n'
              'To remove a task type "Remove"\n'
              'To display the commands again type "Help"\n')
    else:
        print('Not a valid command, check spelling and enter again')
else:
    print('To enter a command please type CORN followed by on the the commands bellow\n'
          'To display your todo list type "Display"\n'
          'To add a new task type "Add"\n'
          'To remove a task type "Remove"\n'
          'To display the commands again type "Help"\n')
